# -*- coding: utf8 -*-
import unittest
from hexagon import lattice


class TestNode(unittest.TestCase):
    def test_init(self):
        number = 10
        cordinates = (0, 0)
        level = 1
        n = lattice.NodeBase(number, cordinates, level)
        self.assertEqual(n.number, number)
        self.assertEqual(n.cordinates, cordinates)
        self.assertEqual(n.level, level)


class TestNodeNeighbors(unittest.TestCase):
    def test_UL_BR(self):
        number = 0
        level = 1
        nodes = [(0, 0), (0, 1), (0, 2), (2, 0), (-1, -1), (-2, -1), (2, 1), (0, -1)]
        neighbors = [(0, 1), (0, 2), (0, 3), (2, 1), (-1, 0), (-2, 0), (2, 2), (0, 0)]
        for i in range(len(nodes)):
            n = lattice.NodeBase(number, nodes[i], level)
            self.assertEqual(n.UL, neighbors[i])

            n = lattice.NodeBase(number, neighbors[i], level)
            self.assertEqual(n.BR, nodes[i])

    def test_L_R(self):
        number = 0
        level = 1
        nodes = [(0, 0), (1, 0), (-1, -2), (-1, 0), (1, -1), (2, -1)]
        neighbors = [(-1, 0), (0, 0), (-2, -2), (-2, 0), (0, -1), (1, -1)]
        for i in range(len(nodes)):
            n = lattice.NodeBase(number, nodes[i], level)
            self.assertEqual(n.L, neighbors[i])

            n = lattice.NodeBase(number, neighbors[i], level)
            self.assertEqual(n.R, nodes[i])

    def test_UR_BL(self):
        number = 0
        level = 1
        nodes = [(0, 0), (-1, 0), (1, 1), (-2, -1), (-1, -2), (-1, -1)]
        neighbors = [(1, 1), (0, 1), (2, 2), (-1, 0), (0, -1), (0, 0)]
        for i in range(len(nodes)):
            n = lattice.NodeBase(number, nodes[i], level)
            self.assertEqual(n.UR, neighbors[i])

            n = lattice.NodeBase(number, neighbors[i], level)
            self.assertEqual(n.BL, nodes[i])


class TestNodeCorGen(unittest.TestCase):
    def setUp(self):
        self.ncg = lattice.NodeCorGen()

    def meta_test(self, levels, head, node, gen):
        for i, lc in enumerate(levels):
            results = gen(i+1)
            for j, result in enumerate(results):
                cls, cor = result
                if j == 0:
                    self.assertEqual(head, cls)
                else:
                    self.assertEqual(node, cls)
                self.assertEqual(cor, lc[j])

    def test_sector_1(self):
        l1 = [(1, 0)]
        l2 = [(2, 0), (2, 1)]
        l3 = [(3, 0), (3, 1), (3, 2)]
        ls = [l1, l2, l3]
        self.meta_test(ls, lattice.S1NodeHead, lattice.S1Node, self.ncg.genSector1)

    def test_sector_2(self):
        l1 = [(1, 1)]
        l2 = [(2, 2), (1, 2)]
        l3 = [(3, 3), (2, 3), (1, 3)]
        ls = [l1, l2, l3]
        self.meta_test(ls, lattice.S2NodeHead, lattice.S2Node, self.ncg.genSector2)

    def test_sector_3(self):
        l1 = [(0, 1)]
        l2 = [(0, 2), (-1, 1)]
        l3 = [(0, 3), (-1, 2), (-2, 1)]
        ls = [l1, l2, l3]
        self.meta_test(ls, lattice.S3NodeHead, lattice.S3Node, self.ncg.genSector3)

    def test_sector_4(self):
        l1 = [(-1, 0)]
        l2 = [(-2, 0), (-2, -1)]
        l3 = [(-3, 0), (-3, -1), (-3, -2)]
        ls = [l1, l2, l3]
        self.meta_test(ls, lattice.S4NodeHead, lattice.S4Node, self.ncg.genSector4)

    def test_sector_5(self):
        l1 = [(-1, -1)]
        l2 = [(-2, -2), (-1, -2)]
        l3 = [(-3, -3), (-2, -3), (-1, -3)]
        ls = [l1, l2, l3]
        self.meta_test(ls, lattice.S5NodeHead, lattice.S5Node, self.ncg.genSector5)

    def test_sector_6(self):
        l1 = [(0, -1)]
        l2 = [(0, -2), (1, -1)]
        l3 = [(0, -3), (1, -2), (2, -1)]
        ls = [l1, l2, l3]
        self.meta_test(ls, lattice.S6NodeHead, lattice.S6Node, self.ncg.genSector6)

    def test_gen_all(self):
        self.assertFalse(self.ncg.genLevel(-1))

        result = self.ncg.genLevel(0)
        self.assertEqual(len(result), 1)
        cls, cor = result[0]
        self.assertEqual(cls, lattice.CentralNode)
        self.assertEqual(cor, (0, 0))

        l1s = [(1, 0), (1, 1), (0, 1), (-1, 0), (-1, -1), (0, -1)]
        l2s = [(2, 0), (2, 1), (2, 2), (1, 2), (0, 2), (-1, 1),
               (-2, 0), (-2, -1), (-2, -2), (-1, -2), (0, -2), (1, -1)]
        l3s = [(3, 0), (3, 1), (3, 2), (3, 3), (2, 3), (1, 3), (0, 3),
               (-1, 2), (-2, 1), (-3, 0), (-3, -1), (-3, -2), (-3, -3),
               (-2, -3), (-1, -3), (0, -3), (1, -2), (2, -1)]

        for i, result in enumerate(self.ncg.genLevel(1)):
            _, cor = result
            self.assertEqual(cor, l1s[i])

        for i, result in enumerate(self.ncg.genLevel(2)):
            _, cor = result
            self.assertEqual(cor, l2s[i])

        for i, result in enumerate(self.ncg.genLevel(3)):
            _, cor = result
            self.assertEqual(cor, l3s[i])


class TestLattice(unittest.TestCase):
    def test_init(self):
        level = 2
        l = lattice.HexgagonalLattice(level)
        self.assertEqual(l.t_level, level)

    def test_nodes_of_level(self):
        l = 2
        hl = lattice.HexgagonalLattice(l)
        results = [1, 6, 12, 18]
        for i in range(len(results)):
            self.assertEqual(hl.nodesOfLevel(i), results[i])

    def test_total_nodes_of_level(self):
        l = 2
        hl = lattice.HexgagonalLattice(l)
        results = [1, 7, 19, 37]
        for i in range(len(results)):
            self.assertEqual(hl.totalNodesOfLevel(i), results[i])

    #def test_add_nodes(self):
        #level = 80
        #hl = lattice.HexgagonalLattice(level)
        #for node in hl.nodes:
            #print node, node.level
        #print hl.N

if __name__ == "__main__":
    unittest.main()
