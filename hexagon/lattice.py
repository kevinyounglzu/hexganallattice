# -*- coding: utf8 -*-


class NodeBase(object):
    def __init__(self, number, cordinates, level):
        self.number = number
        self.cordinates = cordinates
        self.level = level
        self.neighbors = []

    @property
    def UL(self):
        i, j = self.cordinates
        return (i, j+1)

    @property
    def UR(self):
        i, j = self.cordinates
        return (i+1, j+1)

    @property
    def L(self):
        i, j = self.cordinates
        return (i-1, j)

    @property
    def R(self):
        i, j = self.cordinates
        return (i+1, j)

    @property
    def BL(self):
        i, j = self.cordinates
        return (i-1, j-1)

    @property
    def BR(self):
        i, j = self.cordinates
        return (i, j-1)

    def addNeighbor(self, neighbor):
        self.neighbors.append(neighbor)

    def __repr__(self):
        return "<NodeBase %d>" % self.number


class CentralNode(NodeBase):
    def __repr__(self):
        return "<CentralNode>"


class S1NodeBase(NodeBase):
    def __repr__(self):
        i, j = self.cordinates
        return "<S1Node %d @ (%d, %d)>" % (self.number, i, j)


class S1NodeHead(S1NodeBase):
    def getFather(self):
        return [self.L]


class S1Node(S1NodeBase):
    def getFather(self):
        return [self.L, self.BL]


class S2NodeBase(NodeBase):
    def __repr__(self):
        i, j = self.cordinates
        return "<S2Node %d @ (%d, %d)>" % (self.number, i, j)


class S2NodeHead(S2NodeBase):
    def getFather(self):
        return [self.BL]


class S2Node(S2NodeBase):
    def getFather(self):
        return [self.BL, self.BR]


class S3NodeBase(NodeBase):
    def __repr__(self):
        i, j = self.cordinates
        return "<S3Node %d @ (%d, %d)>" % (self.number, i, j)


class S3NodeHead(S3NodeBase):
    def getFather(self):
        return [self.BR]


class S3Node(S3NodeBase):
    def getFather(self):
        return [self.R, self.BR]


class S4NodeBase(NodeBase):
    def __repr__(self):
        i, j = self.cordinates
        return "<S4Node %d @ (%d, %d)>" % (self.number, i, j)


class S4NodeHead(S4NodeBase):
    def getFather(self):
        return [self.R]


class S4Node(S4NodeBase):
    def getFather(self):
        return [self.UR, self.R]


class S5NodeBase(NodeBase):
    def __repr__(self):
        i, j = self.cordinates
        return "<S5Node %d @ (%d, %d)>" % (self.number, i, j)


class S5NodeHead(S5NodeBase):
    def getFather(self):
        return [self.UR]


class S5Node(S5NodeBase):
    def getFather(self):
        return [self.UL, self.UR]


class S6NodeBase(NodeBase):
    def __repr__(self):
        i, j = self.cordinates
        return "<S6Node %d @ (%d, %d)>" % (self.number, i, j)


class S6NodeHead(S6NodeBase):
    def getFather(self):
        return [self.UL]


class S6Node(S6NodeBase):
    def getFather(self):
        return [self.L, self.UL]


class NodeCorGen(object):
    def __init__(self):
        self.sectors = 6

    def genLevel(self, level):
        if level < 0:
            return []
        elif level == 0:
            return [(CentralNode, (0, 0))]
        result = []

        gens = [self.genSector1, self.genSector2, self.genSector3,
                self.genSector4, self.genSector5, self.genSector6]
        for gen in gens:
            for item in gen(level):
                result.append(item)

        return result

    def genSector1(self, level):
        result = [(S1NodeHead, (level, 0))]
        for i in range(1, level):
            result.append((S1Node, (level, i)))
        return result

    def genSector2(self, level):
        result = [(S2NodeHead, (level, level))]
        for i in range(1, level):
            result.append((S2Node, (level-i, level)))
        return result

    def genSector3(self, level):
        result = [(S3NodeHead, (0, level))]
        for i in range(1, level):
            result.append((S3Node, (-i, level-i)))
        return result

    def genSector4(self, level):
        result = [(S4NodeHead, (-level, 0))]
        for i in range(1, level):
            result.append((S4Node, (-level, -i)))
        return result

    def genSector5(self, level):
        result = [(S5NodeHead, (-level, -level))]
        for i in range(1, level):
            result.append((S5Node, (-(level-i), -level)))
        return result

    def genSector6(self, level):
        result = [(S6NodeHead, (0, -level))]
        for i in range(1, level):
            result.append((S6Node, (i, -(level-i))))
        return result


class HexgagonalLattice(object):
    def __init__(self, t_level):
        self.t_level = t_level
        self.node_cor_gen = NodeCorGen()
        self.N = self.totalNodesOfLevel(self.t_level)
        self.nodes = []
        self.addNodes()

    def addNodes(self):
        self.cordinates = []
        number = 0
        for l in range(self.t_level):
            for cls, cor in self.node_cor_gen.genLevel(l):
                self.cordinates.append(cor)
                self.nodes.append(cls(number, cor, l))
                number += 1
        self.rCordinates = dict()
        for i, cor in enumerate(self.cordinates):
            self.rCordinates[cor] = i

    def nodesOfLevel(self, l):
        if l == 0:
            return 1
        else:
            return l * 6

    def totalNodesOfLevel(self, l):
        return 3 * l**2 + 3 * l + 1
